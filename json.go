package service_routes_exporter

import (
	"encoding/json"
	"net/http"
)

type RoutesJsonResponse struct {
	Routes []*RouteJson `json:"routes"`
}

type RouteJson struct {
	Path   string `json:"path"`
	Method string `json:"method"`
}

func NewJsonResponse(rs []*Route) ([]byte, error) {
	resp := &RoutesJsonResponse{
		Routes: make([]*RouteJson, len(rs)),
	}

	for i, r := range rs {
		resp.Routes[i] = &RouteJson{
			Path:   r.Path,
			Method: r.Method,
		}
	}

	return json.Marshal(resp)
}

func (m *RouteManager) HandlerJSON(w http.ResponseWriter, r *http.Request) {
	resp, err := NewJsonResponse(m.GetRoutes())

	if err == nil {
		w.WriteHeader(http.StatusOK)
		w.Header().Add("Content-Type", "application/json")
		w.Write(resp)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
}
