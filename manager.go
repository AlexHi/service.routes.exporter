package service_routes_exporter

import (
	"errors"
	"github.com/gorilla/mux"
	"sync"
)

type RouteManager struct {
	routes []*Route
	mu     sync.RWMutex
	router *mux.Router
}

type Route struct {
	Path   string `json:"path"`
	Method string `json:"method"`
}

func NewManager(r *mux.Router) (*RouteManager, error) {
	if r == nil {
		return nil, errors.New("empty router")
	}

	m := &RouteManager{
		routes: nil,
		mu:     sync.RWMutex{},
		router: r,
	}

	err := m.init()

	if err != nil {
		return nil, err
	}

	return m, nil
}

func (m *RouteManager) GetRoutes() []*Route {
	m.mu.RLock()
	defer m.mu.RUnlock()

	return m.routes
}

func (m *RouteManager) AddRoute(route *mux.Route) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	p, err := route.GetPathTemplate()

	if err != nil {
		return err
	}

	ms, err := route.GetMethods()

	if err != nil {
		return err
	}

	for _, method := range ms {
		r := &Route{
			Path:   p,
			Method: method,
		}

		m.routes = append(m.routes, r)
	}

	return nil
}

func (m *RouteManager) init() error {
	return m.router.Walk(m.walkFn)
}

func (m *RouteManager) walkFn(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
	m.AddRoute(route)
	return nil
}
